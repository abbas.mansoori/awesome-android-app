package com.example.awesomeandroidapp

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.PackageManagerCompat


class JsObject {
    @SuppressLint("RestrictedApi")
    @JavascriptInterface
    fun postMessage(data: String?) {
        Log.v(PackageManagerCompat.LOG_TAG, data!!)
    }
}

class MainActivity : AppCompatActivity() {
    private lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)



        webView = findViewById(R.id.webView)

        webView.webViewClient = WebViewClient()

        webView.loadUrl("https://achmea-nl-2638.app-preview.dev.platform24.se/home")

        webView.settings.javaScriptEnabled = true

        webView.settings.domStorageEnabled = true


        webView.settings.setSupportZoom(true)

        val standardAgent = webView.settings.userAgentString
        webView.settings.userAgentString = "$standardAgent/ ExternalP24NativeApp"

        webView.addJavascriptInterface(JsObject(), "Android")
    }

    override fun onBackPressed() {
        if (webView.canGoBack())
            webView.goBack()
        else
            super.onBackPressed()
    }
}