package com.example.awesomeandroidapp

//import android.annotation.SuppressLint
import android.os.Bundle
//import android.util.Log
//import android.webkit.JavascriptInterface
import android.webkit.WebView
import android.webkit.WebViewClient
//import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
//import androidx.core.content.PackageManagerCompat.LOG_TAG

//class JsObject {
//    @SuppressLint("RestrictedApi")
//    @JavascriptInterface
//    fun shareData(data: String?) {
//        Log.v(LOG_TAG, data!!)
//    }
//}

class SecondActivity : AppCompatActivity() {
    private lateinit var webView: WebView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        webView = findViewById(R.id.webView)

        webView.webViewClient = WebViewClient()

        webView.loadUrl("https://achmea-nl-2638.app-preview.dev.platform24.se/home")

        webView.settings.javaScriptEnabled = true

        webView.settings.setSupportZoom(true)

        webView.addJavascriptInterface(JsObject(), "Android")
    }

    override fun onBackPressed() {
        if (webView.canGoBack())
            webView.goBack()
        else
            super.onBackPressed()
    }
}